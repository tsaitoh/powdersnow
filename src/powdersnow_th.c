#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include "powdersnow_th.h"

/*
 * PowderSnow is a program which can compute 64-bit unique IDs based on time,
 * shard ID, and sequential numbers.  PowderSnow is based on SnowFlake (the
 * twitter ID generator) and the instagram ID generator.  To fit with numerical
 * simulations, the time period has been changed from 41 years (instagram ID
 * generator) to 4 years. This program can generate 256 unique IDs per
 * milliseconds. 
 *
 * This is the thread safe version of PowderSnow. Note that APIs are different
 * from the original version of PowderSnow.
 */

#define BASE_BIT (64)
#define TIME_BIT (37)   // at least 4 years
#define SHARD_BIT (19)  
#define SEQUENCE_BIT (8)
#define SEQUENCE_BASE (1<<8)


//19:01:18 (JST) 9th May 2018 in millisecond.
#define PowderSnowOffset (1525860078344)


void PowderSnowEnvInit(PowderSnowEnv *PS, const int ShardID){
    const int ShardBase = (1<<SHARD_BIT);
    PS->FirstCall = false;
    PS->ShardID = ShardID%ShardBase;
    PS->Offset = PowderSnowOffset;
    PS->LastTime = 0;
    PS->Sequence = 0;
    return ;
}


PowderSnowEnv *PowderSnowEnvAllocator(const int ShardID){
    PowderSnowEnv *ps;

    ps = malloc(sizeof(PowderSnowEnv));

    if(ps == NULL){
        fprintf(stderr,"Memory Allocation Error:%s:line%d:%s()\n",
            __FILE__,__LINE__,__FUNCTION__);
        exit(1);
    }

    PowderSnowEnvInit(ps,ShardID);
    return (ps);
}

void PowderSnowEnvRelease(PowderSnowEnv *PS){
    free(PS);
    return; 
}


/*
 * With this function, you can specify the ShardID value.  The first 19 bits are
 * adopted.
 */
void PowderSnowEnvSetShardID(PowderSnowEnv *PS, const int ShardID){
    const int ShardBase = (1<<SHARD_BIT);
    PS->ShardID = ShardID%ShardBase;
    PS->FirstCall = false;
    return ;
}

/*
 * This function returns milliseconds.
 */
static unsigned long GetMilliseconds(void){
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME,&ts);
    return (ts.tv_sec*1000 + ts.tv_nsec/1000000);
}

/*
 * This function returns a unique ID based on the current time, shard ID, and
 * the sequential number.  The return value is unsigned long (64 bits).  This
 * function can generate 256 unique IDs per milliseconds.
 */
unsigned long PowderSnowIDGenerator_th(PowderSnowEnv *PS){

    unsigned long CurrentTime = GetMilliseconds();
    while(PS->LastTime > CurrentTime){
        usleep(100);
        CurrentTime = GetMilliseconds();
    }

    if(PS->LastTime!=CurrentTime){
        PS->Sequence = 0;
    }else{
        PS->Sequence = (PS->Sequence+1)%SEQUENCE_BASE;
        if(PS->Sequence == 0){
            CurrentTime = GetMilliseconds();
            while(PS->LastTime >= CurrentTime){
                usleep(1);
                CurrentTime = GetMilliseconds();
            }
        }
    }
    PS->LastTime = CurrentTime;
    return (((CurrentTime-PS->Offset) << (BASE_BIT-TIME_BIT)) 
            | (PS->ShardID<<(BASE_BIT-TIME_BIT-SHARD_BIT)) 
            | PS->Sequence);
}
