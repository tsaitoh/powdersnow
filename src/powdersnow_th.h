#include <stdbool.h>

typedef struct StructPowderSnowEnv{
    bool FirstCall;
    int ShardID;
    unsigned long Offset;
    unsigned long LastTime;
    unsigned long Sequence;
} PowderSnowEnv;

void PowderSnowEnvInit(PowderSnowEnv *PS, const int ShardID);
void PowderSnowEnvSetShardID(PowderSnowEnv *PS, const int ShardID);
PowderSnowEnv *PowderSnowEnvAllocator(const int ShardID);
void PowderSnowEnvRelease(PowderSnowEnv *PS);
unsigned long PowderSnowIDGenerator_th(PowderSnowEnv *PS);
