#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <omp.h>
#include "powdersnow_th.h"

static void PowderSnowTest(void){

    fprintf(stderr,"PowderSnow test\n");

    PowderSnowEnv PS;
    PowderSnowEnvInit(&PS,1);
 
    unsigned long int value = PowderSnowIDGenerator_th(&PS);
    fprintf(stderr,"%lu\n",value);

    for(int i=0;i<1000;i++){
        value = PowderSnowIDGenerator_th(&PS);
        if(i%100==0)
            fprintf(stderr,"%lu\n",value);
    }

    PowderSnowEnvSetShardID(&PS,10);
    for(int i=0;i<1000;i++){
        value = PowderSnowIDGenerator_th(&PS);
        if(i%100==0)
            fprintf(stderr,"%lu\n",value);
    }


#define TEST_SIZE (1000000)
    unsigned long *values;
    values = malloc(sizeof(unsigned long)*TEST_SIZE);

    struct timespec ts,te;
    clock_gettime(CLOCK_REALTIME,&ts);
    for(int i=0;i<TEST_SIZE;i++){
        values[i] = PowderSnowIDGenerator_th(&PS);
    }
    clock_gettime(CLOCK_REALTIME,&te);
    double time = (te.tv_sec-ts.tv_sec)*1.0 + 
                  (te.tv_nsec-ts.tv_nsec)*1.e-9;

    free(values);
    fprintf(stderr,"PowderSnow generates %g IDs per sec\n",
            TEST_SIZE/time);
    fprintf(stderr,"Note that the theoretical value is 2.56e5 (=256x10^3) IDs per sec\n");

    return ;
}


static void PowderSnowThreadTest(void){

#pragma omp parallel
    {
#   pragma omp barrier 
#   pragma omp single
        {
            fprintf(stderr,"Number of thread = %d\n",omp_get_num_threads());
        }


        int nthread = omp_get_num_threads();
        int myrank = omp_get_thread_num();
        PowderSnowEnv PS[nthread];
        PowderSnowEnvInit(PS+myrank,myrank);

        fprintf(stderr,"PS.ShardID = %d\n",PS[myrank].ShardID);
        fflush(NULL);

#   pragma omp barrier 
        for(int i=0;i<1000;i++){
            unsigned long value = PowderSnowIDGenerator_th(&PS[myrank]);
            if(i%100==0)
                fprintf(stderr,"%lu [%d]\n",value,myrank);
        }

#   pragma omp barrier 
#   pragma omp single
        {
            fprintf(stderr,"Powdersnow thread parallel ID generation test.\n");
            fprintf(stderr,"This test takes ~4 sec.\n");
        }

#define TEST_SIZE (1000000)
        unsigned long *values;
        values = malloc(sizeof(unsigned long)*TEST_SIZE);

        struct timespec ts,te;
        clock_gettime(CLOCK_REALTIME,&ts);
        for(int i=0;i<TEST_SIZE;i++){
            values[i] = PowderSnowIDGenerator_th(&PS[myrank]);
        }
#   pragma omp barrier 
        clock_gettime(CLOCK_REALTIME,&te);
        double time = (te.tv_sec-ts.tv_sec)*1.0 + 
                      (te.tv_nsec-ts.tv_nsec)*1.e-9;
        free(values);

#   pragma omp barrier 
#   pragma omp single
        {
            fprintf(stderr,"PowderSnow generates %g IDs per sec\n",
                    TEST_SIZE*nthread/time);
            fprintf(stderr,"Note that the theoretical value is %d (=256x10^3*nthread) IDs per sec.\n",
                    (int)(2.56e5*nthread));
        }
    }


    return ;
}

void PowderSnowAllocationTest(void){


    fprintf(stderr,"PowderSnow arg allocation test\n");
    // case 1.
    {
        PowderSnowEnv PS;
        PowderSnowEnvInit(&PS,1);
     
        unsigned long int value = PowderSnowIDGenerator_th(&PS);
        fprintf(stderr,"%lu\n",value);
    }

    // case 2.
    {
        PowderSnowEnv *PS;
        PS = PowderSnowEnvAllocator(1);
     
        unsigned long int value = PowderSnowIDGenerator_th(PS);
        fprintf(stderr,"%lu\n",value);
        PowderSnowEnvRelease(PS);
    }

    return ;
}

int main(const int argc, const char **name){

    PowderSnowTest();
    PowderSnowThreadTest();
    PowderSnowAllocationTest();

    return EXIT_SUCCESS;
}
