#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include "powdersnow.h"

static void PowderSnowTest(void){


    fprintf(stderr,"PowderSnow test\n");
    
    unsigned long int value = PowderSnowIDGenerator();
    fprintf(stderr,"%lu\n",value);

    for(int i=0;i<1000;i++){
        value = PowderSnowIDGenerator();
        if(i%100==0)
            fprintf(stderr,"%lu\n",value);
    }

    PowderSnowSetShardID(10);
    for(int i=0;i<1000;i++){
        value = PowderSnowIDGenerator();
        if(i%100==0)
            fprintf(stderr,"%lu\n",value);
    }

    PowderSnowSetShardIDLowerSevenBits(10);
    for(int i=0;i<1000;i++){
        value = PowderSnowIDGenerator();
        if(i%100==0)
            fprintf(stderr,"%lu\n",value);
    }

    fprintf(stderr,"\nCheck the ID generation rate.\n");
    fprintf(stderr," This check takes ~ 10sec.\n");
#define TEST_SIZE (2500000)
    unsigned long *values;
    values = malloc(sizeof(unsigned long)*TEST_SIZE);

    struct timespec ts,te;
    clock_gettime(CLOCK_REALTIME,&ts);
    for(int i=0;i<TEST_SIZE;i++){
        values[i] = PowderSnowIDGenerator();
    }
    clock_gettime(CLOCK_REALTIME,&te);
    double time = (te.tv_sec-ts.tv_sec)*1.0 + 
                  (te.tv_nsec-ts.tv_nsec)*1.e-9;
    fprintf(stderr,"PowderSnow generates %g IDs per sec\n",
            TEST_SIZE/time);
    fprintf(stderr,"Note that the theoretical value is 2.56e5 (=256x10^3) IDs per sec\n");

    return ;
}

int main(const int argc, const char **name){

    PowderSnowTest();

    return EXIT_SUCCESS;
}
