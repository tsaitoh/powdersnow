# PowderSnow
A software to generate (pseudo) unique IDs.

# Background
When running a particle simulation, I sometimes need a unique ID to give it a
particle (e.g., the cases that a new particle borns or a particle is split into
smaller particles). The straightforward way is to find an unused ID by checking
all particles and then use it. But it is time consuming especially for
massively-parallel simulations.

It is nice if there is a simple way to prepare a unique ID. Here, I note that
there is no need to be a perfectly unique ID, it is okay if the collision rate
is low enough. uuid can satisfy this demand, but uuid_t has a 128-bit length and
it is not easy to use in C. Therefore, I surveyed possible alternatives and
finally, I found that SnowFlake, that was developed by twitter inc. and the
instagram ID are fit for my purpose.

There is room to be improved to be fit with for numerical simulations. I,
therefore, develop PowderSnow. The algorithm is essensially identical to that
used to generate the indstagram ID.

# The structure of the PowderSnow ID
Each PowderSnow ID consists of a 64-bit value. Since this, it should be store an
unsigned long integer.  The structure of a PowderSnow ID can be split into three
segments and they are 37 bits for time, 19 bits for shard, and 8 bits for
sequential number.

The first 37 bits are used to express the time in the units of Milliseconds. The
37 bits can express ~4 years.

The second 19 bits are used for ShardID (the name *shard* comes from the
instagram ID). This should be  machine/thread independent value. 

The last 8 bits are used for the sequential number. This means that PowderSnow
can generate 256 unique IDs per milliseconds.

It is worth noting that the second segment is crucial to guarantee uniqueness
for parallel simulations which can finish within 4 years. PowderSnow prepares
three different ways to set the ShardID.


# How to use this
If you need an ID, call "PowderSnowIDGenerator()". Then this function returns a
64-bit PowderSnow ID. 


## Initialization of ShardID
There are three ways to initialize ShardID. The first way is that the program
automatically sets the values based on the host ID and the process ID. If you do
not call any initialization routines described below, PowderSnow adopts this for
ShardID.


The second way is to set the user specifies value as ShardID via
"PowderSnowSetShardID(user specified number)". All the 19 bits are filled with
it.  

The last is a hybrid one of above two: it uses the user specifies value to fill
the last 7 bits instead of the value evaluated from the process ID and the first
12 bits comes from the host ID. To do this, one needs to call
"PowderSnowSetShardIDLowerSevenBits(user specified number)".

If you are running a thread parallel program, it would be safe to use
"PowderSnowSetShardIDLowerSevenBits()" and put the thread number as the user
specified value. Be careful that the maximum number of threads in a node is 128.

# Sample program  

By typing the make command in "src", a sample program, "test.out", is compiled.
This sample program calls "PowderSnowIDGenerator()" many times and shows the
results.

```
> cd src
> make
> ./test.out
```

